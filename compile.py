import os
import codecs
import markdown
import subprocess
import sys

def GenerateHtml(MdFileName, TemlText):
    md_text = codecs.open(MdFileName, mode="r", encoding="utf-8").read()
    body_text = markdown.markdown(md_text)
    html_file = codecs.open(os.path.splitext(MdFileName)[0] + ".html", "w",
                            encoding="utf-8",
                            errors="xmlcharrefreplace")
    html_file.write(TemlText.replace("{body}", body_text))
    os.remove(MdFileName)

directory = "./"
if len(sys.argv) > 1:
    directory = sys.argv[1]

tmpl_file_on_default = os.path.abspath(os.getcwd() + "/template.html")
tmpl_file_in_repo = os.path.abspath(directory + "/template.html")
if os.path.exists(tmpl_file_in_repo) and tmpl_file_in_repo!=tmpl_file_on_default:
    tmpl_text = codecs.open(tmpl_file_in_repo, mode="r", encoding="utf-8").read()
    os.remove(tmpl_file_in_repo)
else:
    tmpl_text = codecs.open(tmpl_file_on_default, mode="r", encoding="utf-8").read()


md_file_names = subprocess.check_output("find " + directory + " -type f -name \"*.md\"", shell=True).split()

for md_file_name in md_file_names:
    GenerateHtml(md_file_name, tmpl_text)