# README #
Контейнер для простого блога на Markdown разметке, размещенного в Git репозитории
## Установка ##
* Добавить к Git репозиторию ssh ключ без пароля. В целях безопастности можно сгенерировать новый и дать доступ только на чтение
* Скачать Docker Image 
```
#!bash 
docker pull abataloff/mdbloggenerator
```
* Запустить новый контейнер с указанием названия репозитория, пути к репозиторию и пути к ssh ключу
```
#!bash
docker run -d -p 80:80 -v $(pwd)"/key":/var/www/.ssh/id_rsa -e repo_name="blog" -e blog_repo="git@bitbucket.org:abataloff/blog.git" --name my-blog abataloff/mdbloggenerator:latest
```
* Выполнить инициализацию
```
#!bash
docker exec my-blog /bin/bash /var/www/blog-generator/init.sh
```
* Устанавливаем Webhooks на Push в master по адресу myblog.net/update_blog. Данный Webhook будет обновлять блог
* Переходим по пути myblog.net/update_blog для того что бы выполнить обновление блога
* Профит
## Шаблон блога ##
В контейнере уже существует [шаблон](https://bitbucket.org/abataloff/mdbloggenerator/src/f488b6755d8e9d05d352f7e00896fea23f285489/template.html), но если потребуется задать свой, то это можно сделать добавив к корень репозитория файл с именем template.html. Тело страницы блока вставляется в плейсхолдер *{body}*
## Редактирование статей ##
Редактирование статей происходит путем Commit в HEAD Git репозитория блога.
Для редактирования статей можно использовать [WebStorm](https://www.jetbrains.com/webstorm/) с плагином для работы с Markdown. Плагин проверяет синтаксис и имеет функцию предпросмотра MD тексты в HTML.