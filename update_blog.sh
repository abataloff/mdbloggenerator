#!/usr/bin/env bash

WORK_DIR=/var/www/blog-generator/
HTML_DIR=/var/www/html

cd "$WORK_DIR"

blog_repo=$(<blog_repo)
repo_name=$(<repo_name)
REPO_DIR="$WORK_DIR""$repo_name"

rm -rf $repo_name
git clone $blog_repo
python compile.py "$REPO_DIR"
rm -rf "$HTML_DIR"/*
cp -rf "$REPO_DIR"/* "$HTML_DIR"/